<?php


class AdminController extends BaseController
{
    const admin_auth_session_key = 'auth_manager_user';
    protected $page_size = 10;
    protected $page = 1;
    public $user;
    // 不用权限直接使用的
    private $allowAction = array(
        'admin/login/index',
        'admin/login/out',
        'admin/ajax/selectupload',
        'admin/ajax/ueupload',
        'admin/index/index',
        'admin/index/main',
        'admin/users/setpwd',
    );

    // 自动执行
    public function init()
    {
        parent::init();
        $this->isLogin();

        $this->view->assign('static', '/static/admin/');
        $this->view->assign('adminuser', $this->user['userinfo']);
        // 检测权限
        $action = strtolower($this->getModuleName() . '/' . $this->request->getControllerName() . '/' . $this->request->getActionName());
        if (!in_array($action, $this->allowAction)) {
            if (!model_admin_users::check_right($action)) {
                // 无权限
                $this->displayWarn('您没有操作的权限 ' . $this->request->getActionName());
            }
        }
        // 分页和每页数量
        if (intval($this->get('limit'))) $this->page_size = intval($this->get('limit'));
        if (intval($this->get('page'))) $this->page = intval($this->get('page'));
    }

    // 操作日志记录
    protected function operation_log($title, $before_data = array(), $after_data = array())
    {
        $action = strtolower($this->getModuleName() . '/' . $this->request->getControllerName() . '/' . $this->request->getActionName());
        $operation_data = array(
            'userid' => $this->user['userinfo']->id,
            'username' => $this->user['userinfo']->username,
            'uri' => $action,
            'url' => 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"],
            'operation_title' => model_admin_rights::get_right_title($action, $title),
            'time' => time(),
            'before_data' => json_encode($before_data, JSON_UNESCAPED_UNICODE),
            'after_data' => json_encode($after_data, JSON_UNESCAPED_UNICODE),
            'isdo' => 0
        );
        if (strpos($_SERVER["REQUEST_URI"], 'do=list') < 1) $operation_data['isdo'] = 1;
        $operation = new model_admin_operationlogs($operation_data);
        $operation->save();
    }

    /**
     * 检测是否登录
     */
    public function isLogin()
    {
        $user = $this->session->get(self::admin_auth_session_key);
        if ($user) {
            // 已登录
            $this->user = unserialize($user);
        } else {
            // 未登录
            $action = strtolower($this->getModuleName() . '/' . $this->request->getControllerName());
            if ($action != 'admin/login') {
                if (isset($_REQUEST['isajax']) && $_REQUEST['isajax'] == 1) {
                    $this->ajaxresult(1, '您未登录请先登录在使用后台功能!');
                } else {
                    $this->redirect("/admin/login/index");
                }
            }
        }
    }

    protected function ajaxresult($code = 0, $msg = '', $data = array(), $plusparam = array())
    {
        $ajax_result = array(
            'code' => $code,
            'msg' => $msg,
            'data' => $data
        );
        unset($plusparam['code'], $plusparam['msg']);
        die(json_encode(array_merge($ajax_result, $plusparam)));
    }

    // 显示错误界面
    protected function displayWarn($message, $template = 'error', $data = array())
    {
        if (isset($_REQUEST['isajax']) && $_REQUEST['isajax'] == 1) {
            $this->ajaxresult(1, $message, $data);
        }
        $this->view->assign('message', $message);
        $this->view->assign('warn_data', $data);
        $this->view->display(APPLICATION_PATH . "/application/modules/Admin/views/error/{$template}.html");
        exit;
    }

    protected function post($var, $default = null)
    {
        $value = $this->request->getPost($var);
        if (is_array($value)) return $value;
        if (is_string($value)) return trim($value);
        if (!$value) return $default;
    }

    protected function get($var, $default = null)
    {
        $value = $this->request->getQuery($var);
        if (is_array($value)) return $value;
        if (is_string($value)) return trim($value);
        if (!$value) return $default;
    }

    protected function _list($model, $parameters2)
    {
        $page = $this->page;
        $parameters['limit'] = $this->page_size;
        $parameters['order'] = 'id desc';
        $parameters['offset'] = intval($page - 1) * $parameters['limit'];
        $parameters['conditions'] = array('1=1 ');
        $parameters = array_merge($parameters, $parameters2);
        // 搜索条件
        $data = $model::find('all', $parameters);
        // 翻页
        $count = $model::count(array('conditions' => $parameters['conditions']));
        return array(
            'count' => $count,
            'data' => $data
        );
    }
}