<?php

/**
 * 基础Controller
 * @author user
 *
 */
class ApiController extends Yaf_Controller_Abstract
{
    public $request;
    public $response;
    public $return = array(
        'code' => '10001',
        'msg' => '参数错误'
    );
    public $data;
    public $config;
    public $game;

    // 自动执行
    function init()
    {
        Yaf_Dispatcher::getInstance()->disableView();
        $this->request = $this->getRequest();
        $this->response = $this->getResponse();
        //$this->view->assign('module', $this->request->getModuleName());
        //$this->view->assign('controller', $this->request->getControllerName());
        //$this->view->assign('action', $this->request->getActionName());
        $this->check_access();
        $this->config = Yaf_Registry::get('config');
    }

    // 校验协议
    function check_access()
    {
        array(
            'gameid',
            'promoterid',
            'version',
            'encrypt' => 'MD5,SHA1',
            'source' => array('client', 'server'),
            'sign' => 'md5(k1=>v1&k2=v2...#source_secret_key) 32位小写',
            '...'
        );
        // 获取data
        $data = $this->request->getPost('data');
        if (!$data) {
            $data = file_get_contents('php://input');
        }
        $data = base64_decode(urldecode($data));
        $data = json_decode($data, true);
        if (is_array($data) && $data && isset($data['gameid']) && isset($data['sign']) &&
            isset($data['source']) && isset($data['encrypt']) && isset($data['version']) && isset($data['promoterid'])
        ) {
            // && isset($data['promoterid'])  暂时放开
            ksort($data);
            $this->data = $data;
            $game_config = model_game_games::get_game($data['gameid']);
            if ($game_config) {
                $this->game = $game_config;
                $secret_key = $game_config['client_key'];
                if (strtolower($data['source']) == 'server') {
                    // 服务端请求
                    $secret_key = $game_config['server_key'];
                    // 校验IP是否在允许表内
                    if ($game_config['status'] == model_game_games::status_disable) {
                        $this->result(10004, '游戏暂时无法使用');
                    } elseif ($game_config['status'] == model_game_games::status_production) {
                        // 做IP限制
                        $allow_ips = json_decode($game_config['allow_ips'], true);
                        if ($allow_ips && !in_array(Ip::get(), $allow_ips)) {
                            $this->result(10005, '访问受限');
                        }
                    }
                }
                $check_sign = $this->create_sign($data, $secret_key, $this->data['encrypt']);
                if ($check_sign == $data['sign']) {
                    // 协议校验成功
                    return;
                }
                $this->result(10003, '校验失败');
            }
            $this->result(10002, '游戏配置错误');
        }
        $this->result();
    }

    // 校验加密协议
    function create_sign($data, $key, $encrypt = 'md5')
    {
        unset($data['sign']);
        ksort($data);
        $_ = '';
        foreach ($data as $k => $v) {
            if (is_null($v)) $_ .= "{$k}=null&";
            else $_ .= "{$k}={$v}&";
        }
        switch (strtoupper($encrypt)) {
            case 'MD5':
            default:
                return md5(trim($_, '&') . '#' . $key);
        }
    }

    function result($code = '10001', $msg = '参数错误', $data = array())
    {
        $this->return = array(
            'code' => "$code",
            'msg' => $msg,
            'data' => $data
        );
        die(json_encode($this->return, JSON_UNESCAPED_UNICODE));
    }
}