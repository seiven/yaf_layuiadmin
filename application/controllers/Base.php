<?php

/**
 * 基础Controller
 * @author user
 *
 */
class BaseController extends Yaf_Controller_Abstract
{
    public $request;
    public $response;
    public $view;
    public $session;
    public $config;
    protected $ispost;

    // 自动执行
    function init()
    {
        //error_reporting(0);
        $this->view = $this->getView();
        // 设置Controller的模板位置为模块目录下的views文件夹
        $template_dir = APPLICATION_PATH . '/application/modules/' . $this->getModuleName() . '/views/';
        if ($this->getModuleName() == 'Index') $template_dir = APPLICATION_PATH . '/application/views';
        $this->setViewpath($template_dir);
        $this->initView();
        // 各模块静态文件位置
        $static_domain = '/static/';//   '//s.8et.cc/'
        $this->view->assign('static', $static_domain . strtolower($this->getModuleName()));
        $this->view->assign('root_static', $static_domain);
        $this->request = $this->getRequest();
        $this->session = Yaf_Session::getInstance();
        $this->response = $this->getResponse();

        $this->view->assign('module', $this->request->getModuleName());
        $this->view->assign('controller', $this->request->getControllerName());
        $this->view->assign('action', $this->request->getActionName());
        $this->config = Yaf_Registry::get('config');
        $this->ispost = $this->request->isPost();
    }
}