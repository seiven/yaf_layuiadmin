<?php

class ErrorController extends BaseController
{

    // 从2.1开始, errorAction支持直接通过参数获取异常
    public function errorAction($exception)
    {
        // 禁止渲染模板
        //Yaf_Dispatcher::getInstance()->disableView();
        /* error occurs */
        $this->view->assign('static', '/static/admin/');
        $this->view->assign('message', $exception->getMessage());
        $this->view->assign('code', $exception->getCode());
        switch ($exception->getCode()) {
            case YAF_ERR_NOTFOUND_MODULE:
            case YAF_ERR_NOTFOUND_CONTROLLER:
            case YAF_ERR_NOTFOUND_ACTION:
            case YAF_ERR_NOTFOUND_VIEW:
                $this->view->assign('code', 404);
                break;
            default :
                break;
        }
    }

    public function indexAction()
    {

    }
}
