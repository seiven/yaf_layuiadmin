<?php

/**
 * 基础Controller
 * @author user
 *
 */
class FrontController extends BaseController
{
    public $data;
    protected $nologinconfig = array(
        'home_clause','home_forgetpass'
    );

    // 自动执行
    function init()
    {
        parent::init();
        if (!in_array(strtolower($this->request->getControllerName().'_'.$this->request->getActionName()),$this->nologinconfig)){
            $this->check_access();
        }
        //error_reporting(0);
        $this->view = $this->getView();
        // 根据不同的代理商使用不同的前端模板(预留)
        $template_dir = APPLICATION_PATH . '/application/modules/' . $this->getModuleName() . '/views/';
        if ($this->data['promoterid']) {
            // 有推荐人则为无详细功能版本
            $template_dir .= 'default';
        } else {
//            $template_dir .= 'platform';// 暂时不开 渠道版本
            $template_dir .= 'default';
        }
        $this->setViewpath($template_dir);
        $this->initView();
    }

    // 校验协议
    function check_access()
    {
        array(
            'gameid',
            'promoterid',
            'uid',
            'encrypt' => 'MD5,SHA1',
            '...',
            'sign' => 'md5(k1=>v1&k2=v2...#source_secret_key) 32位小写',
        );
        // 获取data
        $data = str_replace(' ', '+', trim($this->request->getQuery('data')));
        $data = base64_decode($data);
        $data = json_decode($data, true);
        if (is_array($data) && $data && isset($data['gameid']) && isset($data['sign']) &&
            isset($data['encrypt']) && isset($data['promoterid']) && isset($data['uid'])
        ) {
            ksort($data);
            $this->data = $data;
            $game_config = model_game_games::get_game($data['gameid']);
            if ($game_config) {
                $secret_key = $game_config['client_key'];
                $check_sign = $this->create_sign($data, $secret_key, $this->data['encrypt']);
                if ($check_sign == $data['sign']) {
                    // 协议校验成功
                    // 写入cookies
                    $this->save_line_user();
                    return;
                }
                $this->error('错误', '参数错误SIGN ERR');
            }
            $this->error('错误', '游戏配置有误');
        } else {
            $this->check_line_user();
        }
    }

    function save_line_user()
    {
        // 写入cookes
        $rand = rand(100000, 999999);
        $sign_array = array(
            'gameid' => $this->data['gameid'],
            'userid' => $this->data['uid'],
            'uid' => $this->data['uid'],
            'promoterid' => $this->data['promoterid'],
            'rand' => $rand,
            'time' => time()
        );
        $sign_array['sign'] = $this->create_sign($sign_array, md5('jidyo82319nkxasln'));
        setcookie('data', base64_encode(json_encode($sign_array)), null, '/');
    }

    function check_line_user()
    {
        $data = isset($_COOKIE['data']) ? $_COOKIE['data'] : null;
        $data = json_decode(base64_decode($data), true);
        if ($data) {
            if (isset($data['sign']) == $this->create_sign($data, md5('jidyo82319nkxasln'))) {
                if (time() - $data['time'] > 1800) {
                    // 超过30分钟过期了
                    $this->error('错误', '您长时间停留未操作,请重新打开界面!');
                }
                $this->data = $data;
                return;
            }
        }
        $this->error('错误', '登录丢失,请重新打开界面!');
    }

    // 校验加密协议
    function create_sign($data, $key, $encrypt = 'md5')
    {
        unset($data['sign']);
        ksort($data);
        $_ = '';
        foreach ($data as $k => $v) {
            $_ .= "{$k}={$v}&";
        }
        switch (strtoupper($encrypt)) {
            case 'MD5':
            default:
                return md5(trim($_, '&') . '#' . $key);
        }
    }

    function result($code = '10001', $msg = '参数错误', $data = array())
    {
        $this->return = array(
            'code' => "$code",
            'msg' => $msg,
            'data' => $data
        );
        die(json_encode($this->return, JSON_UNESCAPED_UNICODE));
    }

    function error($title, $message, $template = 'warn')
    {
        if (isset($_REQUEST['isajax']) && $_REQUEST['isajax'] == 1) {
            $this->result(10001, $message);
        }
        $this->view->assign('title', $title);
        $this->view->assign('message', $message);
        $this->view->display(APPLICATION_PATH . "/application/views/public/{$template}.html");
        exit;
    }
}