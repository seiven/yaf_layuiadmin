<?php

/**
 * 日志
 */
class Logger
{
    static $interface;
    private static $monolog;
    private static $filepath;//= APPLICATION_PATH . '/cache/log/';
    static $islog = false;

    static function getLogInterface()
    {
        if (!self::$monolog) {
            $logger = new \Monolog\Logger('logger');
            $logger->pushHandler(new \Monolog\Handler\StreamHandler(self::$filepath . date('Y-m-d') . '.log', \Monolog\Logger::WARNING));
            self::$monolog = $logger;
        }
        return self::$monolog;
    }

    // 单例
    static function getInterface($logpath = 'error_log')
    {
        self::$filepath = APPLICATION_PATH . "/cache/{$logpath}/";
        if (!self::$interface) self::$interface = new self();
        return self::$interface;
    }

    function __construct()
    {
        // 确认日志开关
        if (isset(Yaf_Registry::get('config')->logger->status)) self::$islog = Yaf_Registry::get('config')->logger->status;

    }

    /**
     * 一定会写入
     * @param $message
     */
    function error($message)
    {
        self::getLogInterface()->error(is_array($message) ? json_encode($message) : $message);
    }

    /**
     * 根据配置写入
     * @param $message
     */
    function log($message)
    {
        if (self::$islog) self::getLogInterface()->error(is_array($message) ? json_encode($message) : $message);
    }
}
