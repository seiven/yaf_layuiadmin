<?php

class Sredis
{
    static $interface;
    public $handle;

    static function getInterface()
    {
        if (is_null(self::$interface)) self::$interface = new self();
        return self::$interface;
    }

    function __construct()
    {
        //Logger::getInterface()->error('REGIDS CONNECTION begin');
        $this->handle = false;
        try {
            $config = new Yaf_Config_Ini(APPLICATION_PATH . '/conf/application.ini');
            $redis = new Predis\Client(array(
                    'scheme' => 'tcp',
                    'host' => $config->product->redis->host,
                    'port' => $config->product->redis->port,
                )
            );
            $redis->auth($config->product->redis->passwd);
            if ($redis->getConnection()) {
                $this->handle = $redis;
            } else {
                Logger::getInterface()->error('REGIDS CONNECTION FAIL');
            }
        } catch (Exception $e) {
            Logger::getInterface()->error('REGIDS CONNECTION FAIL' . json_encode($e->getMessage()));
        }
    }
}