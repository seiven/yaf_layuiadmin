<?php
use ActiveRecord\Model;

class model_admin_groups extends Model
{
    static $table_name = 'admin_group';
    static $primary_key = 'gid';
}
