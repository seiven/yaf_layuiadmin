<?php

class model_admin_operationlogs extends ActiveRecord\Model
{
    // explicit table name since our table is not "books"
    static $table_name = 'admin_operationlogs';

    // explicit pk since our pk is not "id"
    static $primary_key = 'id';

}
