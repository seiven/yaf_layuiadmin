<?php

/**
 * 权限资源
 * @author user
 *
 */
class model_admin_rights extends ActiveRecord\Model
{
    // explicit table name since our table is not "books"
    static $table_name = 'admin_rights';

    // explicit pk since our pk is not "id"
    static $primary_key = 'id';
    static $belongs_to = array(
        array('parent', 'foreign_key' => 'parent_id', 'class_name' => 'model_admin_rights')
    );

    /**
     * 获得所有controller,action
     * @param string $module
     * @return array
     */
    static function getControllers($module = 'Admin')
    {
        $controllerDir = APPLICATION_PATH . "/application/modules/{$module}/controllers";
        if (!is_dir($controllerDir)) return array();
        $controllerList = scandir($controllerDir);
        $controller = array();
        foreach ($controllerList as $file) {
            if (!in_array($file, array(
                '.',
                '..',
                //'Ajax.php',
                'Login.php'
            ))
            ) {
                $content = file_get_contents($controllerDir . '/' . $file);
                if (strpos($content, 'AdminController')) {
                    preg_match_all("/[\S]*Action\(/", $content, $methods);
                    foreach ($methods as $item) {
                        foreach ($item as $action) {
                            $controller[trim($file, '.php')][] = preg_replace('/Action\(/', '', $action);
                        }
                    }
                }
            }
        }
        return $controller;
    }

    /**
     * 获取所有模块列表
     * @return array
     */
    static function getModule()
    {
        $modules = explode(',', Yaf_Registry::get('config')->application->modules);
        return $modules;
    }

    /**
     * @param int $parentid
     */
    static function get_children_right($parentid = 0, $count = -4, $ismerge = true)
    {
        $result = array();
        $count = $count + 4;
        $children = self::all(array('conditions' => array('parent_id = ?', $parentid)));
        if ($children) {
            foreach ($children as $item) {
                $result[$item->id] = array(
                    'id' => $item->id,
                    'parent_id' => $item->parent_id,
                    'name' => $ismerge ? str_repeat('&nbsp;', $count) . $item->name : $item->name,
                    'parent_name' => $item->parent ? $item->parent->name : '-',
                    'children' => self::get_children_right($item->id, $count, $ismerge)
                );
                if ($ismerge) $result = array_merge($result, $result[$item->id]['children']);
            }
        }
        return $result;
    }

    static function get_right_title($uri, $default_title = '')
    {
        $uri = str_replace('/', '@', $uri);
        $allright = self::all();
        foreach ($allright as $right) {
            $right_content = explode(',', strtolower($right->content));
            if ($right->content != strtolower($right->content)) {
                $right->content = strtolower($right->content);
                $right->save();
            }
            if ($right_content && in_array(strtolower($uri), $right_content)) return $right->name;
        }
        return $default_title;
    }
}
