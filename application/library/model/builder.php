<?php

class model_builder
{
    static $interface;
    protected $conn;

    function __construct()
    {
        $config = new Yaf_Config_Ini(APPLICATION_PATH . '/conf/application.ini');
        $this->conn = ActiveRecord\ConnectionManager::get_connection($config->product->database->default_connection);
    }

    static function getConn()
    {
        if (!(self::$interface instanceof model_builder)) self::$interface = new self();
        return self::$interface;
    }

    function createBuilder($table_name)
    {
        return new ActiveRecord\SQLBuilder($this->conn, $table_name);
    }
}
