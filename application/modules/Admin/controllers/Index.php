<?php

/**
 * @name IndexController
 * @desc 默认控制器
 */
class IndexController extends AdminController
{
    public function indexAction()
    {
        // 加载菜单
        $allMenus = model_admin_menus::getMenus();
        foreach ($allMenus as $k => $v) {
            if (isset($v['list']) && $v['list']) {
                foreach ($v['list'] as $k2 => $v2) {
                    if (!model_admin_users::check_right($v2['url'])) unset($allMenus[$k]['list'][$k2]);
                }
                if (isset($allMenus[$k]['list']) && empty($allMenus[$k]['list'])) unset($allMenus[$k]);
            } else {
                // 无下级检测本身
                if (!model_admin_users::check_right($v['url'])) unset($allMenus[$k]);
            }
        }
        $this->view->assign('admin_menus', $allMenus);
        //$this->view->display('index/index2.html');exit;
    }

    public function mainAction()
    {

    }
}