<?php

/**
 * @name LoginController
 * @desc 登录
 */
class LoginController extends AdminController
{
    public function init()
    {
        parent::init();
        if ($this->isLogin()) $this->redirect("/admin/index/index");
    }

    public function indexAction()
    {
        if ($this->request->isPOST()) {
            // 获取参数
            $username = $this->post('username');
            $password = $this->post('password');
            //$isremember = $this->post('isremember', false);
            $user = model_admin_users::find_by_username($username);
            if ($user) {
                // account info ok
                if ($user->password == model_admin_users::hash_password($password, $user->salt)) {
                    // password is right
                    if ($user->status == 1) {
                        // 用户权限
                        $userLoginStatus = model_admin_users::saveLoginStatus($user);
                        if ($userLoginStatus['status'] == true) {
                            //return $this->redirect("/{$this->getModuleName()}/index/index");
                            // 记录日志
                            $this->isLogin();
                            $this->operation_log('后台登陆');
                            $this->ajaxresult(0,'',array(
                                'url'=>"/admin/index/index"
                            ));
                        } else {
                            // 登录失败
                            $this->ajaxresult(1, '登陆失败');
                        }
                    } else {
//                        $this->view->assign('errorMessage', '您的账户被锁定');
                        $this->ajaxresult(1, '您的账户被锁定');
                    }
                }
            }
            $this->ajaxresult(1, '您的账户或密码错误');
        }
    }

    /**
     * 退出
     */
    public function outAction()
    {
        // 记录日志
        $this->operation_log('后台退出');
        $this->session->del('auth_manager_user');
        return $this->redirect("/admin/login/index");
    }
}