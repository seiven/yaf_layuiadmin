<?php

/**
 * 菜单管理
 * @author user
 *
 */
class MenusController extends AdminController
{
    public function indexAction()
    {
        $action = $this->get('do');
        switch ($action) {
            case 'list':
                $this->operation_log('查看管理菜单');
                $parameters['order'] = 'sort desc';
                $parameters['limit'] = $this->page_size;
                $page = $this->get('page', 1);
                $parameters['offset'] = intval($page - 1) * $parameters['limit'];
                $parameters['conditions'] = array('parentid = 0');
                $group_data = model_admin_menus::find('all', $parameters);
                // 翻页
                $count = model_admin_menus::count(array('conditions' => $parameters['conditions']));
                foreach ($group_data as $item) {
                    $data_result[] = array(
                        'id' => $item->id,
                        'title' => $item->title,
                        'sort' => $item->sort,
                        'url' => $item->url,
                        'icon' => $item->icon
                    );
                    if ($item->children) {
                        foreach ($item->children as $child) {
                            $data_result[] = array(
                                'id' => $child->id,
                                'title' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $child->title,
                                'url' => $child->url,
                                'icon' => $child->icon,
                                'sort' => $child->sort,
                            );
                        }
                    }
                }
                $this->ajaxresult(0, '', $data_result, array('count' => $count));
                break;

        }
    }

    /**
     * 新增,编辑
     */
    public function saveAction()
    {
        $id = $this->request->getParam('id');
        if ($id) {
            $data = model_admin_menus::first(array('id' => $id));
            if (!$data) $this->ajaxresult(1, '未找到您要修改的信息');
            $this->view->assign('data', $data);
        }
        if ($this->request->isPost()) {
            $operation_before_data = array();
            // 转成字符串
            $post_data = array(
                'parentid' => $this->post('parentid'),
                'title' => $this->post('title'),
                'icon' => $this->post('icon'),
                'url' => $this->post('url'),
                'sort' => $this->post('sort')
            );
            if ($data) {
                $operation_before_data = array(
                    'parentid' => $data->parentid,
                    'title' => $data->title,
                    'icon' => $data->icon,
                    'url' => $data->url,
                    'id' => $data->id,
                    'sort' => $data->sort,
                );
                $status = $data->update_attributes($post_data);
            } else {
                $data = new model_admin_menus($post_data);
                $status = $data->save();
            }
            if ($status == false) $this->ajaxresult(1, join($data->getMessages(), '<br>'));
            // 记录日志
            $this->operation_log('管理菜单新增或编辑', $operation_before_data, $post_data);
            $this->ajaxresult(0, '操作成功', array('url' => '/Admin/menus/index'));
        }
        // 加载顶级分类
        $this->view->assign('topmenus', model_admin_menus::find('all', array(
            'conditions' => array(
                'parentid=0'
            )
        )));
    }

    /**
     * 删除
     */
    public function delAction()
    {
        $id = $this->request->getParam('id');
        if ($id) {
            if (model_admin_menus::count(array('conditions' => array('parentid = ?', $id)))) {
                $this->ajaxresult(0, '还有下级分类不能删除');
            }
            $roles = model_admin_menus::first(array('id' => $id));
            $roles->delete();
            // 记录日志
            $this->operation_log('管理菜单删除', array('id' => $id), array());
        }
        $this->ajaxresult(0, '操作成功');
    }
}
