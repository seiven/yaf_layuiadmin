<?php

class OperationController extends AdminController
{
    public function indexAction()
    {
        $do = $this->get('do');
        switch ($do) {
            case 'list':
                $this->operation_log('行为日志');
                $parameters['limit'] = $this->page_size;
                $page = $this->page;
                $parameters['offset'] = intval($page - 1) * $parameters['limit'];
                $parameters['conditions'] = ['isdo=1'];
                $data = $this->_list('model_admin_operationlogs', $parameters);
                // 翻页
                foreach ($data['data'] as $item) {
                    $data_result[] = array(
                        'id' => $item->id,
                        'username' => $item->username,
                        'title' => $item->operation_title,
                        'uri' => $item->uri,
                        'time' => date('Y-m-d H:i:s', $item->time),
                    );
                }
                $this->ajaxresult(0, '', $data_result, array('count' => $data['count']));
                break;
        }
    }

    function profileAction()
    {
        $id = $this->get('id');
        $data = model_admin_operationlogs::first(array('id' => $id));
        if (!$data) $this->displayWarn('未找到您要查看的信息');
        $this->view->assign('data', $data);
    }

}
