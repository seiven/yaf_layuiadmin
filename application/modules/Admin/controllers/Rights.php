<?php

/**
 * 权限资源
 * @author user
 *
 */
class RightsController extends AdminController
{
    public function indexAction()
    {
        $action = $this->get('do');
        switch ($action) {
            case 'list':
                // 记录日志
                $this->operation_log('查看权限资源列表');
                $data_result = model_admin_rights::get_children_right();
                $this->ajaxresult(0, '', $data_result, array('count' => $this->page_size));
                break;
        }
    }

    /**
     * 新增,编辑
     */
    public function saveAction()
    {
        $id = $this->request->getParam('id');
        $parent_id = $this->request->getParam('pid');
        if ($id) {
            $rights = model_admin_rights::find('first', array('id' => $id));
            if (!$rights) $this->ajaxresult(1, '未找到您要修改的信息');
            $this->view->assign('allowList', explode(',', strtolower($rights->content)));
            $this->view->assign('data', $rights);
        }
        if ($this->request->isPost()) {
            $operation_before_data = array();
            // 动作集合
            $actionList = $this->post('actionList');
            $actionList = array_unique($actionList);
            // 转成字符串
            $post_data = array(
                'name' => $this->post('rightName'),
                'parent_id' => $this->post('parent_id'),
                'content' => strtolower(join(',', $actionList))
            );
            if ($rights) {
                if ($post_data['parent_id'] == $rights->id) $this->ajaxresult(1, '不能自己作为自己的上级');
                // 自己不能选择自己做自己的上级, 也不能选择自己的下级做为自己的上级
                $chileren = model_admin_rights::get_children_right($rights->id);
                if (isset($chileren[$postData['parent_id']])) $this->ajaxresult(1, '不能选择您的下级作为您的上级');
                $status = $rights->update_attributes($post_data);

                $operation_before_data = array(
                    'name' => $rights->name,
                    'parent_id' => $rights->parent_id,
                    'content' => $rights->content,
                    'id' => $rights->id,
                );
            } else {
                $rights = new model_admin_rights($post_data);
                $status = $rights->save();
            }
            if ($status == false) $this->ajaxresult(1, join($rights->getMessages(), '<br>'));
            $this->operation_log('编辑权限资源', $operation_before_data, $post_data);
            $this->ajaxresult(0, '操作成功', array('url' => '/Admin/Rights/index'));
        }
        // 获得完整的action列表
        $all_module = model_admin_rights::getModule();
        $actionlist = array();
        foreach ($all_module as $module) {
            $controllers = model_admin_rights::getControllers($module);
            if ($controllers) $actionlist[$module] = $controllers;
        }
        $this->view->assign('all_actions', $actionlist);
        $this->view->assign('all_rights', model_admin_rights::get_children_right());
        $this->view->assign('parent_id', intval($parent_id));
    }

    /**
     * 删除
     */
    public function delAction()
    {
        $id = $this->request->getParam('id');
        if (model_admin_rights::count(array('conditions' => array('parent_id = ?', $id))) < 1) {
            $data = model_admin_rights::find('first', array('id' => $id));
            if ($data) $data->delete();
        } else {
            $this->ajaxresult(1, '含下级权限,删除失败');
        }
        $this->operation_log('删除权限资源', array('id' => $id));
        $this->ajaxresult(0, '删除成功');
    }
}
