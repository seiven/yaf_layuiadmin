<?php

/**
 * 角色分组
 * @author user
 *
 */
class RolesController extends AdminController
{
    public function indexAction()
    {
        $action = $this->get('do');
        switch ($action) {
            case 'list':
                $this->operation_log('查看角色列表');
                $parameters['limit'] = $this->page_size;
                $page = $this->page;
                $parameters['offset'] = intval($page - 1) * $parameters['limit'];
                $parameters['conditions'] = array();
                $group_data = model_admin_groups::find('all', $parameters);
                // 翻页
                $count = model_admin_groups::count(array('conditions' => $parameters['conditions']));
                foreach ($group_data as $item) {
                    $data_result[] = array(
                        'id' => $item->gid,
                        'gname' => $item->gname
                    );
                }
                $this->ajaxresult(0, '', $data_result, array('count' => $count));
                break;
        }
    }

    public function saveAction()
    {
        $gid = $this->request->getParam('gid');
        if ($gid) {
            $group = model_admin_groups::find('first', $gid);
            if (!$group) $this->ajaxresult(0, '未找到您要修改的信息');
            $this->view->assign('groupRight', explode(',', $group->rightlist));
            $this->view->assign('data', $group);
        }
        if ($this->request->isPost()) {
            $operation_before_data = array();
            $rights = $this->post('rights');
            $post_data = array(
                'parent_id' => $this->post('parent_id'),
                'gname' => $this->post('gname'),
                'rightlist' => join(',', array_unique($rights ? $rights : array()))
            );
            if ($group) {
                $operation_before_data = array(
                    'parent_id' => $group->parent_id,
                    'gname' => $group->gname,
                    'rightlist' => $group->rightlist,
                );
                unset($post_data['gname']);
                $status = $group->update_attributes($post_data);
            } else {
                $group = new model_admin_groups($post_data);
                $status = $group->save();
            }

            if ($status == false) $this->ajaxresult(1, join($rights->getMessages(), '<br>'));
            $this->operation_log('编辑角色分组', $operation_before_data, $post_data);
            $this->ajaxresult(0, '处理成功');
        }
        // 加载所有分组
        $this->view->assign('groupList', model_admin_groups::find('all'));
        // 加载所有权限资源
        $this->view->assign('allrights', model_admin_rights::get_children_right(0, -4, false));
    }

    public function delAction()
    {
        $gid = $this->request->getParam('gid');
        if ($gid && $gid != 1) {
            $group = model_admin_groups::first(array('gid' => $gid));
            if ($group) {
                $group->delete();
                $this->operation_log('删除角色分组', array('gid' => $gid));
            }
        }
        $this->ajaxresult(0, '删除成功');
    }
}
