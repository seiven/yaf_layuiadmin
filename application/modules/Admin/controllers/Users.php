<?php

class UsersController extends AdminController
{
    public function indexAction()
    {
        $do = $this->get('do');
        switch ($do) {
            case 'list':
                $this->operation_log('查看后台用户列表');
                $parameters['limit'] = $this->page_size;
                $page = $this->page;
                $parameters['offset'] = intval($page - 1) * $parameters['limit'];
                $parameters['conditions'] = array();
                $group_data = model_admin_users::find('all', $parameters);
                // 翻页
                $count = model_admin_users::count(array('conditions' => $parameters['conditions']));
                foreach ($group_data as $item) {
                    $data_result[] = array(
                        'id' => $item->id,
                        'username' => $item->username,
                        'email' => $item->email,
                        'group_name' => ($item->group) ? $item->group->gname : '-',
                        'truename' => $item->truename,
                        'status' => $item->status
                    );
                }
                $this->ajaxresult(0, '', $data_result, array('count' => $count));
                break;
        }
    }

    /**
     * set password
     */
    public function setpwdAction()
    {
        if ($this->post()) {
            $old_password = $this->post('old_password');
            $new_password = $this->post('new_password');
            $confirm_password = $this->post('confirm_password');
            if ($old_password && $new_password && $confirm_password && $new_password == $confirm_password) {
                // 检测旧密码
                $user = model_admin_users::first($this->user['userinfo']->id);
                if ($user) {
                    if (model_admin_users::hash_password($old_password, $user->salt) == $user->password) {
                        $salt = rand(111111, 999999);
                        $user->salt = $salt;
                        $user->password = model_admin_users::hash_password($new_password, $salt);
                        $user->save();
                        $this->operation_log('修改密码');
                        $this->ajaxresult(0, '修改成功', array('url' => '/admin/login/out'));
                    } else {
                        $this->ajaxresult(1, '用户旧密码错误');
                    }
                } else {
                    $this->ajaxresult(1, '用户登录有误');
                }
            } else {
                $this->ajaxresult(1, '旧密码及两次输入新密码必须一致');
            }
        }
    }

    /**
     * 保存管理员用户
     */
    public function saveAction()
    {
        $id = $this->request->getParam('id');
        if ($id) {
            $data = model_admin_users::first(array('id' => $id));
            if (!$data) $this->ajaxresult(1, '您要更新的账户不存在');
            $this->view->assign('data', $data);
        }
        if ($this->request->isPost()) {
            $operation_before_data = array();
            $postData = array(
                'username' => $this->post('username'),
                'password' => $this->post('password'),
                'email' => $this->post('email'),
                'phone' => $this->post('phone'),
                'createtime' => time(),
                'status' => $this->post('status') == 'on' ? 1 : 0,
                'groupid' => $this->post('groupid'),
                'truename' => $this->post('truename'),
                'avatar' => $this->post('avatar')
            );

            if (empty($postData['groupid'])) {
                $this->ajaxresult(1, '请选择用户所属用户角色分组');
            } elseif (is_null($postData['password']) && !$id) {
                // 新增无密码;
                $this->ajaxresult(1, '新增用户必须填入密码');
            }
            if (!empty($postData['password'])) {
                $postData['salt'] = rand(100000, 999999);
                $postData['password'] = md5(md5($postData['password']) . $postData['salt']);
            }
            if ($data) {
                $operation_before_data = array(
                    'username' => $data->username,
                    'email' => $data->email,
                    'phone' => $data->phone,
                    'status' => $data->status,
                    'groupid' => $data->groupid,
                    'truename' => $data->truename,
                    'avatar' => $data->avatar,
                );
                // 更新
                unset($postData['username'], $postData['createtime']);
                if (empty($postData['password'])) {
                    // 新增无密码
                    unset($postData['password']);
                }
                if ($this->user['userinfo']->id == $data->id) unset($postData['status']);
                $status = $data->update_attributes($postData);
            } else {
                // 新增
                // 判断账户是否存在
                $hasUser = model_admin_users::count(array(
                    'username' => $postData['username']
                ));
                if ($hasUser) $this->ajaxresult(1, '用户已存在无法新增');
                $data = new model_admin_users($postData);
                $status = $data->save();
            }
            if ($status == false) $this->ajaxresult(1, '用户操作失败');
            $this->operation_log('修改管理员用户', $operation_before_data, $postData);
            $this->ajaxresult(0, '操作成功', array('url' => '/admin/users/index'));
        }
        // 角色分组
        $this->view->assign('groupList', model_admin_groups::all());
    }

    public function delAction()
    {
        $id = $this->request->getParam('id');
        if ($id == $this->user['userinfo']->id) $this->ajaxresult(1, '自己无法删除自己的账户');
        if ($id) {
            $data = model_admin_users::first(array('id' => $id));
            if ($data) {
                $data->delete();
                $this->operation_log('删除管理员用户', array('id' => $id));
            }
        }
        $this->ajaxresult(0, '操作成功');
    }
}
