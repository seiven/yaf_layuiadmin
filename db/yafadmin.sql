-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';

DROP DATABASE IF EXISTS `yafadmin`;
CREATE DATABASE `yafadmin` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `yafadmin`;

DELIMITER ;;

CREATE PROCEDURE `refresh_account_role`(IN `rid` bigint unsigned)
begin
   set @role_auth = 0;
end;;

DELIMITER ;

CREATE TABLE `admin_group` (
  `gid` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `gname` varchar(30) DEFAULT NULL COMMENT '用户组名称',
  `rightlist` text COMMENT '权限列表',
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理员分组';


CREATE TABLE `admin_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentid` int(11) DEFAULT '0',
  `title` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT 'icon-desktop',
  `url` varchar(50) DEFAULT '/Admin/index/main',
  `sort` int(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parentid` (`parentid`),
  KEY `sort` (`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='后台管理菜单';


CREATE TABLE `admin_operationlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(255) NOT NULL DEFAULT '' COMMENT '管理员登陆号',
  `uri` varchar(255) NOT NULL DEFAULT '' COMMENT 'URI',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT 'URL',
  `operation_title` varchar(255) NOT NULL DEFAULT '' COMMENT '操作标题',
  `time` int(11) NOT NULL DEFAULT '0' COMMENT '提交时间',
  `before_data` text NOT NULL COMMENT '处理前数据',
  `after_data` text NOT NULL COMMENT '处理后数据',
  `isdo` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否有具体操作',
  PRIMARY KEY (`id`),
  KEY `userid_time` (`userid`,`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理员操作行为日志';


CREATE TABLE `admin_rights` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(6) unsigned NOT NULL DEFAULT '0',
  `name` varchar(120) DEFAULT NULL COMMENT '权限名称',
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限资源';


CREATE TABLE `admin_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型0:图片1:文件2',
  `file_name` varchar(255) DEFAULT NULL COMMENT '文件名称',
  `file_size` int(4) NOT NULL DEFAULT '0' COMMENT '文件大小',
  `file_url` varchar(255) NOT NULL DEFAULT '' COMMENT '文件地址',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '上传时间',
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传文件列表';


CREATE TABLE `admin_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT '' COMMENT '用户名',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码 ',
  `salt` char(6) NOT NULL DEFAULT '' COMMENT '密码随机值',
  `email` varchar(255) DEFAULT NULL COMMENT '邮件地址',
  `createtime` bigint(11) DEFAULT '0' COMMENT '创建时间',
  `lasttime` bigint(11) DEFAULT '0' COMMENT '最后登录时间',
  `lastip` varchar(80) DEFAULT NULL COMMENT '最后登录IP',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '是否禁用   0禁用  1正常',
  `groupid` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '分组编号',
  `phone` bigint(11) unsigned DEFAULT '0' COMMENT '电话',
  `savelog` text,
  `truename` varchar(10) DEFAULT NULL COMMENT '真实姓名',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像图标',
  `user_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '用户类型0:系统用户1:代理商',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员账户';


-- 2018-08-29 04:02:37