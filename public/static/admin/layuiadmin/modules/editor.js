layui.define(['wangEditor'], function (exports) {
    var obj = {
        simple: function (ele) {
            var $text1 = layui.jquery(ele + '-textarea')
            var editor = new layui.wangEditor(ele);
            editor.customConfig = {
                menus: ['head', 'bold', 'fontSize', 'fontName', 'strikeThrough', 'foreColor', 'backColor'],
                fontNames: ['宋体', '微软雅黑', 'Arial'],
                colors: ['#000000', '#FF0000', '#1c487f', '#4d80bf', '#c24f4a', '#8baa4a', '#7b5ba1', '#46acc8', '#f9963b', '#ffffff'],
                height: '100px'
            }
            // 下面两个配置，使用其中一个即可显示“上传图片”的tab。但是两者不要同时使用！！！
            // editor.customConfig.uploadImgShowBase64 = true   // 使用 base64 保存图片
            // editor.customConfig.uploadImgServer = '/upload'  // 上传图片到服务器

            editor.customConfig.onchange = function (html) {
                // 监控变化，同步更新到 textarea
                $text1.val(html)
            }
            editor.create()
            // 初始化 textarea 的值
            $text1.val(editor.txt.html());
        },
        full: function (ele) {
            var $text1 = layui.jquery(ele + '-textarea')
            var editor = new layui.wangEditor(ele);
            // 下面两个配置，使用其中一个即可显示“上传图片”的tab。但是两者不要同时使用！！！
            // editor.customConfig.uploadImgShowBase64 = true   // 使用 base64 保存图片
            // editor.customConfig.uploadImgServer = '/upload'  // 上传图片到服务器

            editor.customConfig.onchange = function (html) {
                // 监控变化，同步更新到 textarea
                $text1.val(html)
            }
            editor.create()
            // 初始化 textarea 的值
            $text1.val(editor.txt.html());
        }
    }
    exports('editor', obj);
});


// [
//     'head',  // 标题
//     'bold',  // 粗体
//     'fontSize',  // 字号
//     'fontName',  // 字体
//     'italic',  // 斜体
//     'underline',  // 下划线
//     'strikeThrough',  // 删除线
//     'foreColor',  // 文字颜色
//     'backColor',  // 背景颜色
//     'link',  // 插入链接
//     'list',  // 列表
//     'justify',  // 对齐方式
//     'quote',  // 引用
//     'emoticon',  // 表情
//     'image',  // 插入图片
//     'table',  // 表格
//     'video',  // 插入视频
//     'code',  // 插入代码
//     'undo',  // 撤销
//     'redo'  // 重复
// ]